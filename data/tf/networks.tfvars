network1 = {
  name = "network10"
  cidr = "10.10.10.0/24"
  bridge = "virbr31"
  type = "none"
}

network2 = {
  name = "network20"
  cidr = "10.10.20.0/24"
  bridge = "virbr32"
  type = "none"
}

network3 = {
  name = "network30"
  cidr = "10.10.30.0/24"
  bridge = "virbr33"
  type = "none"
}

network4 = {
  name = "network40"
  cidr = "10.10.40.0/24"
  bridge = "virbr34"
  type = "none"
}

network-ext = {
  name = "tf-network-ext"
  type = "bridge"
  bridge = "br0"
}

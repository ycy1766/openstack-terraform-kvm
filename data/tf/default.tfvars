#kvm_provider_url = "qemu+ssh://root@192.168.18.3/system"
kvm_provider_url = "qemu+ssh://root@127.0.0.1/system"

cloud_image_dir = "../images"
cloud_image_file = "CentOS-7-x86_64-GenericCloud-1611.qcow2"

vm_vol_pool_dir = "/data01/VMs-path"
vm_vol_pool_name = "VMs-pool"

#vm_pub_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHy3RKNEJUJqyqVCllb2Ph6/a5r7j//VJze4VZXWvuRrpDtUCJzz/HtZYq9F06IOwPDwblruelhxSotxd1gdZDBuP1u+9zjoP8t8aTvLPDsyoy2lDnp2AsuhJTE1v0YGIVt3ydmoknTb+8jMj8bY4CuaOj1Y8WJtMKYSgP6uzj6tSPDCquGp3+5t7MP8G2acwKvLyNK6a85MZv60gIjn2R/z/hK2bOj1xB77PUWZw6Dbjhf9BUOHFKqVDD2nkYYIA7RJLuKjG1sS5u8mkEuv5LBEzpV4JswqyPfDAJKcr4kFrv1IZ/8EpdMvY9MdBOIsaWkg47QxaZpGsZGHJy+Wx/ root@jyy-openstack"
vm_pub_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTlTBmpATq+LJldJBoT8zp17B+qQvFnvxZVPtdZd0jiN5V/PbAlXMbVCu1wkc+bDxctIT9vrLw8YurjdJFt91KNZ/SZH4q0+NQ62879qxAMJi3fqFM4ASoBr3LieT2/tL85MGWYxBWFqysPIY1EV5qn6+RFN0y80JpetJIBqRXYr7iXD9yIdX7dDEkrMWOK712m9a6mPYYpU2YvJz8G7mmMI/TuTEXyY3egbEGFn+YE2oYiiJYG1W1NEGoyWjI77Nc7GsLMfsMCWo6Zw/gafv3zqj5arm/NI+w/brL94xqyeR6ObjGNHeXFOF8J/qp/8GyzhaNqgg7FcW8scteMDTT root@openstack_test"
vm_user_data = "#cloud-config\npassword: password\nchpasswd: { expire: False }\nssh_pwauth: True"

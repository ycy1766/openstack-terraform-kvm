vm_controller_name = "controller"
vm_controller_names = [
  "controller01",
  "controller02",
  "controller03"
]

vm_controller_type = {
  cpu = "2"
  memory = "11240"
  root_disk_size = "40"
}

vm_controller_ip_addresses = [
  "10.10.10.11",
  "10.10.10.12",
  "10.10.10.13"
]

vm_controller_mac_addresses = [
  "32:87:02:43:df:a1",
  "32:87:02:43:df:a2",
  "32:87:02:43:df:a3"
]


vm_compute_name = "compute"
vm_compute_names = [
  "compute01",
  "compute02",
  "compute03"
]

vm_compute_type = {
  cpu = "3"
  memory = "11240"
  root_disk_size = "40"
}

vm_compute_ip_addresses = [
  "10.10.10.21",
  "10.10.10.22",
  "10.10.10.23"
]

vm_compute_mac_addresses = [
  "32:87:02:43:df:b1",
  "32:87:02:43:df:b2",
  "32:87:02:43:df:b3"
]
